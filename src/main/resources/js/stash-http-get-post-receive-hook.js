var aeffle = {};

aeffle.getTextInput = function (id) {
    'use strict';
    var input, inputName = {
        url: 'input#url',
        useAuth: 'input#useAuth',
        user: 'input#user',
        pass: 'input#pass'
    };

    if (id > 1) {
        inputName.url += id;
        inputName.useAuth += id;
        inputName.user += id;
        inputName.pass += id;
    }

    input = {
        url: AJS.$(inputName.url),
        useAuth: AJS.$(inputName.useAuth),
        user: AJS.$(inputName.user),
        pass: AJS.$(inputName.pass)
    };

    input.setInputEditableStatus = function (isEnabled) {
        var classAttribute = (isEnabled ? 'text disabled' : 'text'),
            disabledAttribute = (isEnabled ? null : 'true');

        this.user.attr('disabled', disabledAttribute).attr('class', classAttribute);
        this.pass.attr('disabled', disabledAttribute).attr('class', classAttribute);
    };
    input.enable = function () {input.setInputEditableStatus(true); };
    input.disable = function () {input.setInputEditableStatus(false); };

    input.update = function () {
        if (input.useAuth.prop('checked')) {
            input.enable();
        } else {
            input.disable();
        }
    };

    return input;
};


aeffle.checkAndHideButtons = function () {
    'use strict';

    var locationCount = AJS.$('input#locationCount').val(),
        minusButton = AJS.$('button#removeTabButton'),
        plusButton = AJS.$('li#add-tab');

    if (locationCount == 1) {
        minusButton.attr('aria-disabled', 'true');
        minusButton.removeClass('important-button');
    } else {
        minusButton.removeAttr('aria-disabled');
        minusButton.addClass('important-button');
    }
    minusButton.blur();
    // force re-plot because solely adding and removing the attribute didn't work
    minusButton.parent().hide().show(0);


    if (locationCount >= 10) {
        plusButton.addClass('hidden');
    } else {
        plusButton.removeClass('hidden');
    }

}

aeffle.addNewTab = function () {
    'use strict';
    var addNewTabItem = AJS.$('li#add-tab'),
        paneDiv = AJS.$('div.horizontal-tabs'),
        newTabContent,
        newPaneContent,
        newPaneValues,
        locationCount = AJS.$('input#locationCount'),
        newId = parseInt(locationCount.val(), 10) + 1;

    if (newId > 10) {
        return;
    }

    newTabContent = de.aeffle.stash.plugin.hook.menuItem({'id': newId, 'isActive': false, 'isLastRemaining': false});

    newPaneValues = {
                        'id': newId,
                        'isActive': false,
                        'config': {}
                    };

    newPaneContent = de.aeffle.stash.plugin.hook.tabsPane(newPaneValues);
    locationCount.val(newId);

    addNewTabItem.before(newTabContent);
    paneDiv.append(newPaneContent);

    aeffle.wireUpUseAuthCheckBox(newId);
    aeffle.wireUpHelpDialog();

    aeffle.checkAndHideButtons();

    // stupid work-around ... otherwise the add-button is the active li element :-/
    setTimeout(function () {
            AJS.$('li#menu-item-' + newId + ' a').attr('aria-select', 'true').click();
        }, 10);
};




aeffle.wireUpUseAuthCheckBox = function (id) {
    'use strict';
    var input = aeffle.getTextInput(id);
    input.useAuth.on('change', input.update);
    input.useAuth.trigger('change');
    input.url.css('max-width', '500px');
};


aeffle.wireUpRemoveTabButton = function () {
    'use strict';
    var removeTabButton,
        removeTabHtml = de.aeffle.stash.plugin.hook.deleteButton();

    if (AJS.$('button#removeTabButton').length != 0) {
        return;
    }

    removeTabButton = AJS.$('div.dialog-button-panel').prepend(removeTabHtml);

    removeTabButton.on('click', function () {
        'use strict';
        var activeTabText,
            activeTabId;

        activeTabText = AJS.$('ul.tabs-menu > li > a[aria-selected="true"] > strong ').html();
        activeTabId = parseInt(activeTabText, 10);

        aeffle.removeTab(activeTabId);
    });
};


aeffle.wireUpNewTabButton = function () {
    'use strict';
    AJS.$('a#add-tab').on('click', aeffle.addNewTab);
};


aeffle.wireUpDebugButton = function () {
    'use strict';
    AJS.$('a#show-debug').on('click', aeffle.showSavedData);
};


aeffle.showDebug = function () {
    'use strict';
    AJS.$('li.hidden').removeClass('hidden');
};


aeffle.showSavedData = function () {
    'use strict';
    var groupId = "de.aeffle.stash.plugin",
        artifactId = "stash-http-get-post-receive-hook",
        repositoryHookKey = "http-get-post-receive-hook",
        url = document.URL +
              "/" + groupId +
              "." + artifactId +
              ":" + repositoryHookKey +
              "/settings";

    AJS.$.getJSON(url, function (data) {
        var niceString = JSON.stringify(data, null, 4);
        alert(niceString);
    });
};


aeffle.wireUpHelpDialog = function () {
    aeffle.wireUpHelpTranslation();
    aeffle.wireUpHelpFilters();
};


aeffle.wireUpHelpTranslation = function () {
    'use strict';
    var helpMessage = de.aeffle.stash.plugin.hook.helpTranslation();

    AJS.InlineDialog(AJS.$("a#show-help-translation"), 1,
        function (content, trigger, showPopup) {
            'use strict';

            content.addClass('avr-help').removeAttr("style").html(helpMessage);
            showPopup();
            return false;
        }
    );
};

aeffle.wireUpHelpFilters = function () {
    'use strict';
    var helpMessage = de.aeffle.stash.plugin.hook.helpFilters();

    AJS.InlineDialog(AJS.$("a#show-help-filters"), 1,
        function (content, trigger, showPopup) {
            'use strict';

            content.addClass('avr-help').removeAttr("style").html(helpMessage);
            showPopup();
            return false;
        }
    );
};

aeffle.showError = function (message) {
    'use strict';
    AJS.messages.error({
        title: "Configuration error!",
        body: message
    });
};


aeffle.swapConfig = function (i, j) {
    'use strict';
    var inputNames = ['select#httpMethod',
                      'input#url',
                      'input#useAuth',
                      'input#user',
                      'input#pass',
                      'input#tagFilter',
                      'input#branchFilter',
                      'input#userFilter',
                      'textarea#postData'];

    inputNames.map(function (inputName) {
        var input1 = (i === 1 ? inputName : inputName + i),
            input2 = (j === 1 ? inputName : inputName + j),
            tmpValue;

        tmpValue = AJS.$(input1).val();
        AJS.$(input1).val(AJS.$(input2).val());
        AJS.$(input2).val(tmpValue);
    });
};


aeffle.removeTab = function (id) {
    'use strict';
    var locationCountItem = AJS.$('input#locationCount'),
        locationCount = parseInt(locationCountItem.val(), 10),
        i,
        lastTabName = 'div#tab-' + locationCount,
        lastMenuItem = 'li#menu-item-' + locationCount;

    if (locationCount === 1) {
        return;
    }

    for (i = id; i < locationCount; i += 1) {
        aeffle.swapConfig(i, i + 1);
    }

    AJS.$(lastMenuItem).remove();
    AJS.$(lastTabName).remove();
    locationCountItem.val(locationCount - 1);
    AJS.$('li#menu-item-1 a').click();

    aeffle.checkAndHideButtons();

    i = parseInt(id, 10);
    i = ( i === 1 ? 1 : i - 1);
    AJS.$('li#menu-item-' + i + ' > a').click();
};



