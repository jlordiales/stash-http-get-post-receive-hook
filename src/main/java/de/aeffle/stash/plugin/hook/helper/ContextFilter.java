/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.helper;


import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.user.StashAuthenticationContext;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationTranslated;

import java.util.regex.PatternSyntaxException;

public class ContextFilter {

    private final String userSlug;
    private final String refId;
    private final String refName;
    // private final String type;

    public ContextFilter(StashAuthenticationContext stashAuthenticationContext,
                         RefChange refChange) {
        this.userSlug = stashAuthenticationContext.getCurrentUser().getSlug();

        // e.g. "refs/heads/master" or "refs/tags/my_tag"
        this.refId = refChange.getRefId();

        this.refName = refChange.getRefId().replaceAll("^refs/(tags|heads)/", "");
        // e.g. "UPDATE"
        // this.type = refChange.getType().toString();

    }

    public boolean checkIfFilterMatches(HttpLocationTranslated httpLocationTranslated) {
        String branchFilter = httpLocationTranslated.getBranchFilter();
        String tagFilter = httpLocationTranslated.getTagFilter();
        String userFilter = httpLocationTranslated.getUserFilter();

        if (userFilterMatches(userFilter)) {
            if (isBranch() && branchFilterMatches(branchFilter))
                return true;

            if (isTag() && tagFilterMatches(tagFilter))
                return true;
        }
        return false;
    }

    private boolean isBranch() {
        return refId.contains("heads");
    }

    private boolean branchFilterMatches(String branchFilter) {
        return checkIfFilterMatches(refName, branchFilter);
    }

    private boolean isTag() {
        return refId.contains("tags");
    }

    private boolean tagFilterMatches(String tagFilter) {
        return checkIfFilterMatches(refName, tagFilter);
    }

    private boolean userFilterMatches(String userFilter) {
        return checkIfFilterMatches(userSlug, userFilter);
    }

    /*
     * Check Filter will be true if:
     *  - the regex in the filter matches the data
     *  - the filter is empty
     *  - the regex is invalid (ignore errors)
     */
    private boolean checkIfFilterMatches(String data, String filter) {
        if (filter.isEmpty()) {
            return true;
        }

        try {
            return data.matches(filter);
        } catch (PatternSyntaxException e) {
            return true;
        }
    }
}
