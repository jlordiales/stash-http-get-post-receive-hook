/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.validators;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import com.atlassian.stash.server.ApplicationPropertiesService;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;
import com.atlassian.stash.user.StashAuthenticationContext;

import de.aeffle.stash.plugin.hook.HttpGetPostReceiveHook;


public class OldUrlValidatorTest {
	
	private HttpGetPostReceiveHook httpGetPostReceiveHook;
	
	private Settings settings;
	private SettingsValidationErrors errors;
	private Repository repository;

	@Before
	public void beforeTestCreateCleanPlayground() {
        ApplicationPropertiesService applicationPropertiesService = mock(ApplicationPropertiesService.class);
		StashAuthenticationContext stashAuthenticationContext = mock(StashAuthenticationContext.class);
		httpGetPostReceiveHook = new HttpGetPostReceiveHook(applicationPropertiesService, stashAuthenticationContext);
		
		settings = mock(Settings.class);
		errors = mock(SettingsValidationErrors.class);
		repository = mock(Repository.class);

        setBranchFilter(1,"");
        setTagFilter(1, "");
        setUserFilter(1, "");

        setBranchFilter(2,"");
        setTagFilter(2, "");
        setUserFilter(2, "");

        setBranchFilter(3,"");
        setTagFilter(3, "");
        setUserFilter(3, "");
	}

	private void setLocationCount(String count) {
		when(settings.getString("locationCount", "1")).thenReturn(count);
	}
	
	private void setUrl(int id, String url) {
		String urlName = ( id > 1 ? "url" + id : "url" );
		when(settings.getString(urlName, "")).thenReturn(url);
	}

    private void setBranchFilter(int id, String data) {
        setFilter(id, "branchFilter", data);
    }

    private void setTagFilter(int id, String data) {
        setFilter(id, "tagFilter", data);
    }

    private void setUserFilter(int id, String data) {
        setFilter(id, "userFilter", data);
    }

    private void setFilter(int id, String filter, String data) {
        String filterName = (id > 1 ? filter + id : filter);
        when(settings.getString(filterName, "")).thenReturn(data);
    }
	
	@Test
	public void testValidateWithEmptyUrl() {
		setLocationCount("1");
		setUrl(1, "");

		httpGetPostReceiveHook.validate(settings, errors, repository);
		
		verify(errors, times(1)).addFieldError(eq("url"), anyString());
	}

	
	@Test
	public void testValidateWithInvalidUrl() {
		setLocationCount("1");
		setUrl(1, "not a valid url");
		
		httpGetPostReceiveHook.validate(settings, errors, repository);

		verify(errors, atLeastOnce()).addFieldError(eq("url"), anyString());
	}
	
	@Test
	public void testValidateWithValidUrl() {
		setLocationCount("1");
		setUrl(1, "http://www.testing.com/valid.url");
		
		httpGetPostReceiveHook.validate(settings, errors, repository);

		verify(errors, never()).addFieldError(anyString(), anyString());
	}
	
	@Test
	public void testValidateWithEmptyUrl2() {
		setLocationCount("2");
		setUrl(1, "");
		setUrl(2, "");
		
		httpGetPostReceiveHook.validate(settings, errors, repository);
		
		verify(errors, times(1)).addFieldError(eq("url2"), anyString());
	}

	
	@Test
	public void testValidateWithInvalidUrl2() {
		setLocationCount("2");
		setUrl(1, "");
		setUrl(2, "not a valid url");
		
		httpGetPostReceiveHook.validate(settings, errors, repository);

		verify(errors, atLeastOnce()).addFieldError(eq("url2"), anyString());
	}

	@Test
	public void testValidateWith2ValidUrl() {
		setLocationCount("2");
		setUrl(1, "http://www.testing.com/valid.url");
		setUrl(2, "http://www.testing.com/valid.url");
		
		httpGetPostReceiveHook.validate(settings, errors, repository);

		verify(errors, never()).addFieldError(anyString(), anyString());
	}
	
	@Test
	public void testValidateWithEmptyUrl3() {
		setLocationCount("3");
		setUrl(1, "");
		setUrl(2, "");
		setUrl(3, "");
		
		httpGetPostReceiveHook.validate(settings, errors, repository);
		
		verify(errors, times(1)).addFieldError(eq("url3"), anyString());
	}

	
	@Test
	public void testValidateWithInvalidUrl3() {
		setLocationCount("3");
		setUrl(1, "");
		setUrl(2, "");
		setUrl(3, "not a valid url");
		
		httpGetPostReceiveHook.validate(settings, errors, repository);

		verify(errors, atLeastOnce()).addFieldError(eq("url3"), anyString());
	}

	@Test
	public void testValidateWith3ValidUrls() {
		setLocationCount("3");
		setUrl(1, "http://www.testing.com/valid.url");
		setUrl(2, "http://www.testing.com/valid.url");
		setUrl(3, "http://www.testing.com/valid.url");
		
		httpGetPostReceiveHook.validate(settings, errors, repository);

		verify(errors, never()).addFieldError(anyString(), anyString());
	}
	

}
