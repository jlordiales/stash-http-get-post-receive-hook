/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.helper;

import static org.junit.Assert.assertEquals;

import com.atlassian.stash.server.ApplicationPropertiesService;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationTranslated;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationUntranslated;
import org.junit.Before;
import org.junit.Test;

import ut.de.aeffle.stash.plugin.hook.testHelpers.ApplicationPropertiesServiceMockFactory;
import ut.de.aeffle.stash.plugin.hook.testHelpers.RefChangeMockFactory;
import ut.de.aeffle.stash.plugin.hook.testHelpers.RepositoryHookContextMockFactory;
import ut.de.aeffle.stash.plugin.hook.testHelpers.StashAuthenticationContextMockFactory;

import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.user.StashAuthenticationContext;

import de.aeffle.stash.plugin.hook.helper.UrlTemplateTranslator;


public class UrlTemplateTranslatorTest {

    private ApplicationPropertiesServiceMockFactory applicationPropertiesServiceMockFactory = new ApplicationPropertiesServiceMockFactory();
	private RepositoryHookContextMockFactory contextFactory = new RepositoryHookContextMockFactory();
	private StashAuthenticationContextMockFactory authenticationContextFactory = new StashAuthenticationContextMockFactory();
	private RefChangeMockFactory refChangeMockFactory = new RefChangeMockFactory();

    @Before
    public void beforeTestClearSettings() {
        applicationPropertiesServiceMockFactory.clear();
    	contextFactory.clear();
    	authenticationContextFactory.clear();
    	refChangeMockFactory.clear();
	}


    @Test
    public void testTransformWithBaseUrl() {
        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();
        RepositoryHookContext repositoryHookContext = contextFactory.getContext();
        RefChange refChange = null;

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("${baseUrl}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

        assertEquals("http://example.com/stash", httpLocationTranslated.getUrl());
    }

    @Test
    public void testTransformWithBaseUrlProtocol() {
        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();
        RepositoryHookContext repositoryHookContext = contextFactory.getContext();
        RefChange refChange = null;

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("${baseUrl.protocol}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

        assertEquals("http", httpLocationTranslated.getUrl());
    }

    @Test
    public void testTransformWithBaseUrlHost() {
        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();
        RepositoryHookContext repositoryHookContext = contextFactory.getContext();
        RefChange refChange = null;

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("${baseUrl.host}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

        assertEquals("example.com", httpLocationTranslated.getUrl());
    }

    @Test
    public void testTransformWithBaseUrlPath() {
        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();
        RepositoryHookContext repositoryHookContext = contextFactory.getContext();
        RefChange refChange = null;

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("${baseUrl.path}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

        assertEquals("/stash", httpLocationTranslated.getUrl());
    }

	@Test
	public void testTransformWithDisplayName() {
 		authenticationContextFactory.setDisplayName("John_Doe");

        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();
		RepositoryHookContext repositoryHookContext = contextFactory.getContext();
		RefChange refChange = null;
		
		UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);

        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("http://doe.com/${user.displayName}");

		HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

		assertEquals("http://doe.com/John_Doe", httpLocationTranslated.getUrl());
	}

    @Test
    public void testTransformWithSpaceInDisplayName() {
        authenticationContextFactory.setDisplayName("John Doe");

        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();
        RepositoryHookContext repositoryHookContext = contextFactory.getContext();
        RefChange refChange = null;

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);

        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("http://doe.com/${user.displayName}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

        assertEquals("http://doe.com/John+Doe", httpLocationTranslated.getUrl());
    }


    @Test
	public void testTransformWithUserName() {
 		authenticationContextFactory.setName("john.doe");

        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();
		RepositoryHookContext repositoryHookContext = contextFactory.getContext();
		RefChange refChange = null;
		
		UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("http://doe.com/${user.name}");

		HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

		assertEquals("http://doe.com/john.doe", httpLocationTranslated.getUrl());
	}
	
	@Test
	public void testTransformWithEmail() {
 		authenticationContextFactory.setEmailAddress("john@doe.de");

        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = authenticationContextFactory.getContext();
		RepositoryHookContext repositoryHookContext = contextFactory.getContext();
		RefChange refChange = null;
		
		UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("http://doe.com/${user.email}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

		assertEquals("http://doe.com/john@doe.de", httpLocationTranslated.getUrl());
	}
	
	@Test
	public void testTransformWithRepository() {
 		contextFactory.setRepositoryId(1);
 		contextFactory.setRepositoryName("Test Repository");
 		contextFactory.setRepositorySlug("REPO");


        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = null;
		RepositoryHookContext repositoryHookContext = contextFactory.getContext();
		RefChange refChange = null;
		
		UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("http://doe.com/${repository.id}/${repository.name}/${repository.slug}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

		assertEquals("http://doe.com/1/Test+Repository/REPO", httpLocationTranslated.getUrl());
	}

	@Test
	public void testTransformWithProject() {
    	contextFactory.setProjectKey("PROJECT");
    	contextFactory.setProjectName("Test Project");

        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = null;
		RepositoryHookContext repositoryHookContext = contextFactory.getContext();
		RefChange refChange = null;
		
		UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("http://doe.com/${project.name}/${project.key}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

		assertEquals("http://doe.com/Test+Project/PROJECT", httpLocationTranslated.getUrl());
	}


    @Test
    public void testTransformWithProjectLower() {
        contextFactory.setProjectKey("PROJECT");

        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = null;
        RepositoryHookContext repositoryHookContext = contextFactory.getContext();
        RefChange refChange = null;

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setUrlTemplate("${project.key.lower}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

        assertEquals("project", httpLocationTranslated.getUrl());
    }

    @Test
    public void testTransformWithRefChange() {
        refChangeMockFactory.setRefId("refs/heads/master");
        refChangeMockFactory.setFromHash("affe");
        refChangeMockFactory.setToHash("cafe");

        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = null;
        RepositoryHookContext repositoryHookContext = null;
        RefChange refChange = refChangeMockFactory.getRefChange();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setHttpMethod("GET");
        httpLocationUntranslated.setUser("john.doe");

        httpLocationUntranslated.setUrlTemplate("http://doe.com/${refChange.refId}/${refChange.fromHash}/${refChange.toHash}/${refChange.type}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

        assertEquals("http://doe.com/refs/heads/master/affe/cafe/UPDATE", httpLocationTranslated.getUrl());
        assertEquals("GET", httpLocationTranslated.getHttpMethod());
        assertEquals("john.doe", httpLocationTranslated.getUser());
    }
    
    @Test
    public void testUrlEncodeRefChangeName() {
	refChangeMockFactory.setRefId("refs/heads/feature/test");
	refChangeMockFactory.setFromHash("affe");
	refChangeMockFactory.setToHash("cafe");

	ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory
		.getContext();
	StashAuthenticationContext stashAuthenticationContext = null;
	RepositoryHookContext repositoryHookContext = null;
	RefChange refChange = refChangeMockFactory.getRefChange();

	UrlTemplateTranslator translator = new UrlTemplateTranslator(
		applicationPropertiesService, stashAuthenticationContext,
		repositoryHookContext, refChange);

	HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
	httpLocationUntranslated.setHttpMethod("GET");
	httpLocationUntranslated.setUser("john.doe");

	httpLocationUntranslated
		.setUrlTemplate("http://doe.com/${refChange.name}");

	HttpLocationTranslated httpLocationTranslated = translator
		.translate(httpLocationUntranslated);

	assertEquals("http://doe.com/feature%2Ftest",
		httpLocationTranslated.getUrl());
	assertEquals("GET", httpLocationTranslated.getHttpMethod());
	assertEquals("john.doe", httpLocationTranslated.getUser());
    }

    @Test
    public void testTransformerCopiesAllValues() {
        refChangeMockFactory.setRefId("refs/heads/master");
        refChangeMockFactory.setFromHash("affe");
        refChangeMockFactory.setToHash("cafe");

        ApplicationPropertiesService applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        StashAuthenticationContext stashAuthenticationContext = null;
        RepositoryHookContext repositoryHookContext = null;
        RefChange refChange = refChangeMockFactory.getRefChange();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService, stashAuthenticationContext, repositoryHookContext, refChange);


        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();
        httpLocationUntranslated.setHttpMethod("GET");
        httpLocationUntranslated.setPostData("post data string");

        httpLocationUntranslated.setUseAuth(true);
        httpLocationUntranslated.setUser("john.doe");
        httpLocationUntranslated.setPass("secret");

        httpLocationUntranslated.setBranchFilter("branchFilter");
        httpLocationUntranslated.setTagFilter("tagFilter");
        httpLocationUntranslated.setUserFilter("userFilter");

        httpLocationUntranslated.setUrlTemplate("http://doe.com/${refChange.refId}/${refChange.fromHash}/${refChange.toHash}/${refChange.type}");

        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);

        assertEquals("http://doe.com/refs/heads/master/affe/cafe/UPDATE", httpLocationTranslated.getUrl());

        assertEquals("GET", httpLocationTranslated.getHttpMethod());
        assertEquals("post data string", httpLocationTranslated.getPostData());

        assertEquals(true, httpLocationTranslated.getUseAuth());
        assertEquals("john.doe", httpLocationTranslated.getUser());
        assertEquals("secret", httpLocationTranslated.getPass());

        assertEquals("branchFilter", httpLocationTranslated.getBranchFilter());
        assertEquals("tagFilter", httpLocationTranslated.getTagFilter());
        assertEquals("userFilter", httpLocationTranslated.getUserFilter());
    }
}